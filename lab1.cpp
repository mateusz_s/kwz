#include <iostream>
#include <memory>
#include <vector>
#include <fstream>
#include <string>

/*class Graph {
std::unique_ptr<Node> graphRoot;
public:
Graph() {
graphRoot.reset(new Node);
}
};*/

struct Node {

    std::vector<std::shared_ptr<Node> > childNodes;
    std::vector<std::weak_ptr<Node> > parentNodes;
    int es;
    int ef;
    int ls;
    int lf;
    int time;

    void addConnectionToChildNode(std::shared_ptr<Node>);
    void addConnectionToParentNode(std::shared_ptr<Node>);
};

void Node::addConnectionToChildNode(std::shared_ptr<Node> childNode) {
    for (auto node : childNodes) {
        if (node == childNode) {
            std::cout << "GOWNO - Co za debil dodal 2 razy to samo polaczenie - dropping" << std::endl;
        }
    }
    childNodes.push_back(childNode);
}

void Node::addConnectionToParentNode(std::shared_ptr<Node> parentNode) {
    parentNodes.push_back(parentNode);
}

void addAllNodesWithoutParentToRoot(std::unique_ptr<Node>& graphNode, std::vector<std::shared_ptr<Node> > &nodes) {
    for(auto node : nodes)
    {
        if(node->parentNodes.size() == 0)
        {
            graphNode->addConnectionToChildNode(node);
        }
    }
}

void addAllNodesWithoutChildToEnd(std::unique_ptr<Node>& graphEnd, std::vector<std::shared_ptr<Node> > &nodes) {
    for (auto node : nodes)
    {
        if (node->childNodes.size() == 0)
        {
            graphEnd->addConnectionToChildNode(node);
        }
    }
}



int main() {
    std::unique_ptr<Node> graphRoot(new Node());
    std::unique_ptr<Node> graphEnd(new Node());
    //   std::shared_ptr<Node> node1(new Node()), node2(new Node());
    //   graphRoot->addConnectionToChildNode(node1);
    //    {
    //       graphRoot->addConnectionToChildNode(node1);	
    //   }

    std::ifstream myFile;

    std::string fileName;
    std::cin >> fileName;

    myFile.open(fileName);


    if (!myFile.is_open())
    {
        std::cerr << "Could not open file" << std::endl;
        return false;
    }

    auto nrOfTasks = 0, nrOfConnections = 0;
    myFile >> nrOfTasks;
    myFile >> nrOfConnections;
    std::vector<std::shared_ptr<Node> > nodes;

    for (auto i = 0; i < nrOfTasks; i++)
    {
        std::shared_ptr<Node> node(new Node());
        myFile >> node->time;
        nodes.push_back(node);
    }

    for (auto i = 0; i < nrOfConnections; i++)
    {
        int parentNode, childNode;
        myFile >> parentNode >> childNode;
        nodes[--parentNode]->addConnectionToChildNode(nodes[--childNode]);
        nodes[childNode]->addConnectionToParentNode(nodes[parentNode]);
    }
 //   addAllNodesWithoutChildToEnd(graphEnd, )
     for(auto node : nodes)
    {
        std::cout << node->time << std::endl;
        for(auto childNode : node->childNodes)
        {
            std::cout << "child: " << childNode->time << std::endl;
        }
        std::cout << "===========================" << std::endl;
    } 

    myFile.close();
    std::cin >> nrOfConnections;
}